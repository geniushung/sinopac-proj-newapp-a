package com.example.testnestscrollview;


import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.uiautomator.UiDevice;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private static final String PACKAGE_NAME = "com.sinopac.ismartstock";

    private final static int SLEEP_SEC = 30000;

    private UiDevice mDevice;

    private float mDensity;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void init() {

        mDevice = UiDevice.getInstance(getInstrumentation());

        mDensity = mActivityTestRule.getActivity().getResources().getDisplayMetrics().density;
    }

    @Test
    public void mainActivityTest() {

        int h = mDevice.getDisplayHeight();
        int w = mDevice.getDisplayWidth();

        try { Thread.sleep(1500); }
        catch (InterruptedException e) { e.printStackTrace(); }

        mDevice.swipe(w / 2, h/ 2, w / 2, h/ 2 - (int)(100 * mDensity), 18);

        try { Thread.sleep(1500); }
        catch (InterruptedException e) { e.printStackTrace(); }

        mDevice.swipe(w / 2, h/ 2, w / 2, h/ 2 - (int)(100 * mDensity), 18);

        try { Thread.sleep(1500); }
        catch (InterruptedException e) { e.printStackTrace(); }

        mDevice.swipe(w / 2, h/ 2, w / 2, h/ 2 - (int)(100 * mDensity), 18);

        try { Thread.sleep(1500); }
        catch (InterruptedException e) { e.printStackTrace(); }
    }
}
